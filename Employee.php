<?php

class Employee
{
    //Свойства
    public $name; //Имя сотрудника
    public $last_name; //Фамилия сотрудника
    public $surname; //Отчество сотрудника
    public $date; //Дата рождения
    public $post; // Должность
    public $id; //Идентификатор сотрудника
    private $salary; // Оклад


    //Методы
    public function _construct_(
        $name,
        $last_name,
        $surname,
        $date,
        $post,
        $id,
        $salary


    )
    {
        $this->name = $name;
        $this->last_name = $last_name;
        $this->surname = $surname;
        $this->date = $date;
        $this->post = $post;
    }

// Установить оклад
    public function setSalary($salary)
    {
        $this->salary = $salary;
    }

// Установить Идентификатор сотрудника

    public function setId($id)
    {
        $this->id = $id;
    }

// Показать оклад сотрудника
    public function getSalary()
    {
        return $this->salary;
    }


}

